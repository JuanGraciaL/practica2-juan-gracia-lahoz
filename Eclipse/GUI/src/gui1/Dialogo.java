package gui1;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JRadioButton;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JTree;
import javax.swing.JMenuItem;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.SpinnerListModel;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Checkbox;
import java.awt.ScrollPane;
import java.awt.Scrollbar;
import java.awt.TextArea;
import java.awt.Panel;
import java.awt.List;
import java.awt.Label;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import java.awt.Toolkit;

public class Dialogo extends JDialog {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Dialogo dialog = new Dialogo();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Dialogo() {
		setFont(new Font("Segoe UI Light", Font.PLAIN, 14));
		setTitle("Alpha");
		setIconImage(Toolkit.getDefaultToolkit().getImage("D:\\Juanito\\Escritorio\\CLASE\\Entornos\\Workspace - Entornos\\GUI\\src\\gui1\\vista.png"));
		getContentPane().setBackground(new Color(255, 160, 122));
		setBounds(100, 100, 684, 442);
		getContentPane().setLayout(null);
		
		JLabel lblRutaAbsoluta = new JLabel("Ruta absoluta");
		lblRutaAbsoluta.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		lblRutaAbsoluta.setBounds(10, 15, 82, 13);
		getContentPane().add(lblRutaAbsoluta);
		
		JSpinner spinner = new JSpinner();
		spinner.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		spinner.setModel(new SpinnerDateModel(new Date(1548802800000L), null, null, Calendar.DAY_OF_WEEK_IN_MONTH));
		spinner.setToolTipText("");
		spinner.setBounds(10, 127, 105, 20);
		getContentPane().add(spinner);
		
		JLabel lblNewLabel = new JLabel("Fecha nacimiento");
		lblNewLabel.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		lblNewLabel.setBounds(10, 111, 176, 13);
		getContentPane().add(lblNewLabel);
		
		JRadioButton rdbtnCrearCarpeta = new JRadioButton("Crear carpeta");
		rdbtnCrearCarpeta.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		rdbtnCrearCarpeta.setBackground(new Color(255, 160, 122));
		rdbtnCrearCarpeta.setBounds(365, 12, 105, 21);
		getContentPane().add(rdbtnCrearCarpeta);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(new Color(220, 220, 220));
		btnCancelar.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		btnCancelar.setBounds(575, 344, 85, 21);
		getContentPane().add(btnCancelar);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		btnAceptar.setBackground(new Color(220, 220, 220));
		btnAceptar.setBounds(480, 344, 85, 21);
		getContentPane().add(btnAceptar);
		
		JButton btnFormatearOrdenador = new JButton("Formatear ordenador");
		btnFormatearOrdenador.setBackground(new Color(220, 220, 220));
		btnFormatearOrdenador.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		btnFormatearOrdenador.setBounds(10, 344, 151, 21);
		getContentPane().add(btnFormatearOrdenador);
		
		JSlider slider = new JSlider();
		slider.setBackground(new Color(255, 160, 122));
		slider.setToolTipText("");
		slider.setBounds(280, 352, 159, 13);
		getContentPane().add(slider);
		
		JLabel lblcuntoEstsDe = new JLabel("\u00BFCu\u00E1nto est\u00E1s de seguro?");
		lblcuntoEstsDe.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		lblcuntoEstsDe.setBounds(280, 330, 200, 20);
		getContentPane().add(lblcuntoEstsDe);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(new Color(220, 220, 220));
		comboBox.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"V", "M", "NSNC"}));
		comboBox.setBounds(10, 80, 64, 21);
		getContentPane().add(comboBox);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		lblSexo.setBounds(10, 66, 46, 13);
		getContentPane().add(lblSexo);
		
		JLabel lblpermisoDelGobierto = new JLabel("\u00BFPermiso del gobierto?");
		lblpermisoDelGobierto.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		lblpermisoDelGobierto.setBounds(10, 150, 151, 20);
		getContentPane().add(lblpermisoDelGobierto);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		comboBox_1.setBackground(new Color(220, 220, 220));
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Si", "No", "El presidente es mi padre"}));
		comboBox_1.setToolTipText("");
		comboBox_1.setBounds(10, 170, 151, 21);
		getContentPane().add(comboBox_1);
		
		Checkbox checkbox = new Checkbox("\u00BFEst\u00E1s seguro?");
		checkbox.setFont(new Font("Segoe UI Light", Font.PLAIN, 10));
		checkbox.setBounds(167, 344, 108, 21);
		getContentPane().add(checkbox);
		
		JTextPane txtpnElUsoQue = new JTextPane();
		txtpnElUsoQue.setEditable(false);
		txtpnElUsoQue.setBackground(new Color(255, 228, 196));
		txtpnElUsoQue.setFont(new Font("Segoe UI Light", Font.PLAIN, 11));
		txtpnElUsoQue.setText("El uso que cada usuario le de al  programa no es de nuestra incumbencia, por lo tanto, las consecuencias que pueda ocasionar un mal uso de este poco currado software, son \u00FAnica y  exclusivamente del usuario que lo est\u00E9 usando. Todo esto va por la opci\u00F3n de formateo, no nos hacemos responsables de que te quedes sin ordenador.");
		txtpnElUsoQue.setBounds(10, 205, 231, 123);
		getContentPane().add(txtpnElUsoQue);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPane.setEnabled(false);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(86, 10, 271, 24);
		getContentPane().add(scrollPane);
		
		JTextPane textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(280, 66, 374, 237);
		getContentPane().add(scrollPane_1);
		
		JTree tree = new JTree();
		tree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("C:\\\t") {
				{
					DefaultMutableTreeNode node_1;
					DefaultMutableTreeNode node_2;
					DefaultMutableTreeNode node_3;
					node_1 = new DefaultMutableTreeNode("Archivos de programa");
						node_1.add(new DefaultMutableTreeNode("Eclipse Lunar"));
						node_1.add(new DefaultMutableTreeNode("Eclipse Solar"));
						node_1.add(new DefaultMutableTreeNode("ProPaint 3.0"));
						node_1.add(new DefaultMutableTreeNode("Visual Studio"));
						node_1.add(new DefaultMutableTreeNode("Villaneitor"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Usuarios");
						node_2 = new DefaultMutableTreeNode("Juanito\t");
							node_2.add(new DefaultMutableTreeNode("Canon"));
							node_2.add(new DefaultMutableTreeNode("Clase"));
							node_2.add(new DefaultMutableTreeNode("Equipo"));
							node_2.add(new DefaultMutableTreeNode("Virus para clase"));
						node_1.add(node_2);
						node_2 = new DefaultMutableTreeNode("Paulina Rubio");
							node_2.add(new DefaultMutableTreeNode("Fotos pre-operasion"));
							node_2.add(new DefaultMutableTreeNode("Fotos post-operasion"));
							node_2.add(new DefaultMutableTreeNode("Palabras para aprender"));
						node_1.add(node_2);
						node_2 = new DefaultMutableTreeNode("Albert Rivera");
							node_2.add(new DefaultMutableTreeNode("Contactos coca"));
							node_2.add(new DefaultMutableTreeNode("Falsos presupuestos (elecciones)"));
							node_2.add(new DefaultMutableTreeNode("Presupuestos reales"));
						node_1.add(node_2);
						node_2 = new DefaultMutableTreeNode("Mr.T");
							node_2.add(new DefaultMutableTreeNode("Tablas gym"));
							node_3 = new DefaultMutableTreeNode("Fotos");
								node_3.add(new DefaultMutableTreeNode("Con Murdock \u2665"));
								node_3.add(new DefaultMutableTreeNode("My niggas"));
							node_2.add(node_3);
						node_1.add(node_2);
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Ejercicios f\u00EDsica cu\u00E1ntica aplicada");
						node_1.add(new DefaultMutableTreeNode("Rubias"));
						node_1.add(new DefaultMutableTreeNode("Morenas"));
						node_1.add(new DefaultMutableTreeNode("Pelirojas"));
						node_1.add(new DefaultMutableTreeNode("Tortitas de pl\u00E1tano"));
					add(node_1);
				}
			}
		));
		scrollPane_1.setViewportView(tree);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		menuBar.add(mnArchivo);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mntmAbrir.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmCargar = new JMenuItem("Cargar");
		mntmCargar.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		mnArchivo.add(mntmCargar);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar estado");
		mntmGuardar.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmExportar = new JMenuItem("Exportar");
		mntmExportar.setBackground(new Color(154, 205, 50));
		mntmExportar.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		mnArchivo.add(mntmExportar);
		
		JMenu mnNewMenu = new JMenu("Instalaci\u00F3n");
		mnNewMenu.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmPausar = new JMenuItem("Pausar");
		mntmPausar.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		mnNewMenu.add(mntmPausar);
		
		JMenuItem mntmReanudar = new JMenuItem("Reanudar");
		mntmReanudar.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		mnNewMenu.add(mntmReanudar);
		
		JMenuItem mntmCancelarInstalacin = new JMenuItem("Cancelar instalaci\u00F3n");
		mntmCancelarInstalacin.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		mnNewMenu.add(mntmCancelarInstalacin);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		mnNewMenu.add(mntmSalir);
		
		JMenu mnNewMenu_1 = new JMenu("Dise\u00F1o");
		mnNewMenu_1.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmVentana = new JMenuItem("Ventana");
		mntmVentana.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		mnNewMenu_1.add(mntmVentana);
		
		JMenuItem mntmTemas = new JMenuItem("Temas");
		mntmTemas.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		mnNewMenu_1.add(mntmTemas);
		
		JMenuItem mntmBuscarEnLnea = new JMenuItem("Buscar en l\u00EDnea");
		mntmBuscarEnLnea.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		mnNewMenu_1.add(mntmBuscarEnLnea);
		
		JMenu mnNewMenu_2 = new JMenu("Ayuda");
		mnNewMenu_2.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmAyudaOnline = new JMenuItem("Ayuda online");
		mntmAyudaOnline.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		mnNewMenu_2.add(mntmAyudaOnline);
		
		JMenuItem mntmBiblioteca = new JMenuItem("Biblioteca");
		mntmBiblioteca.setFont(new Font("Segoe UI Light", Font.PLAIN, 13));
		mnNewMenu_2.add(mntmBiblioteca);
	}
}
